﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ConsoleApplication8
{
    class Program
    {
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern int MessageBox(IntPtr hWnd, string text, string caption, uint type);

        [DllImport("wininet.dll", CharSet = CharSet.Unicode)]
        public static extern long InternetOpen(string agent, long accessType, string proxyName, string proxyBypass, long flags);

        [DllImport("wininet.dll", CharSet = CharSet.Unicode)]
        public static extern long InternetConnect(long internetHandle, string serverName, int serverPort, string username, string password, long service, long flags, long context);

        [DllImport("wininet.dll", CharSet = CharSet.Unicode)]
        public static extern bool FtpSetCurrentDirectory(long session, string directory);

        static void Main(string[] args)
        {
            int ftpService = 1;
            string ftpServer = "ftp.udngroup.com";
            int port = 0;
            string username = "adv2114";
            string password = "2114npwl";
            int passiveMode = 0x08000000;

            long internetHandle = InternetOpen("Weird", 1, null, null, 0);
            if (internetHandle == 0)
            {
                // fail
                //
                MessageBox(new IntPtr(0), "Fail to create internet connection handle", "Info", 0);
                return;
            }
            long internetSession = InternetConnect(internetHandle, ftpServer, port, username, password, ftpService, passiveMode, 0);
            if (internetSession == 0)
            {
                // fail
                //
                MessageBox(new IntPtr(0), "Fail to create ftp session", "Info", 0);
                return;
            }
            if (!FtpSetCurrentDirectory(internetSession, "ADV"))
            {
                // fail
                //
                MessageBox(new IntPtr(0), "Fail to change directory", "Info", 0);
                return;
            }
            MessageBox(new IntPtr(0), "Change current directory to ADV", "Info", 0);
        }
    }
}
